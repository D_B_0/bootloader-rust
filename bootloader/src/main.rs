#![no_std]
#![no_main]

extern crate alloc;
use crate::alloc::{vec, vec::Vec};

use alloc::string::String;
use log::*;
use uefi::{
    prelude::*,
    proto::media::{
        file::{File, FileAttribute, FileInfo, FileMode, RegularFile},
        fs::SimpleFileSystem,
    },
    table::{
        boot::{MemoryType, PAGE_SIZE},
        runtime::ResetType,
    },
    CStr16,
};

#[allow(dead_code)]
fn get_memory_map(system_table: &mut SystemTable<Boot>) {
    let memory_map_size = system_table.boot_services().memory_map_size();
    info!("Memory map size: {memory_map_size:?}");
    let mut buffer =
        Vec::<u8>::with_capacity(memory_map_size.map_size + 8 * memory_map_size.entry_size);
    unsafe {
        buffer.set_len(buffer.capacity());
    }
    info!(
        "buffer size: {} len, {} cap",
        buffer.len(),
        buffer.capacity()
    );
    let mmap = system_table
        .boot_services()
        .memory_map(&mut buffer)
        .expect("couldn't retrieve memory map");
    let mmap = mmap.entries().copied().collect::<Vec<_>>();
    info!("efi: usable memory ranges ({} total)", mmap.len());
    for descriptor in mmap {
        match descriptor.ty {
            MemoryType::CONVENTIONAL => {
                let size = descriptor.page_count * PAGE_SIZE as u64;
                let end_address = descriptor.phys_start + size;
                info!(
                    "> {:#x} - {:#x} ({} KiB)",
                    descriptor.phys_start, end_address, size
                );
            }
            _ => {}
        }
    }
}

fn get_file_size(file: &mut RegularFile) -> usize {
    let mut info_buf = vec![];
    let min_size = file
        .get_info::<FileInfo>(&mut info_buf)
        .unwrap_err()
        .data()
        .unwrap();
    info_buf.resize(min_size, 0);
    let file_info = file.get_info::<FileInfo>(&mut info_buf).unwrap();
    file_info.file_size() as usize
}

fn get_file_raw_content(file: &mut RegularFile) -> Vec<u8> {
    let mut file_content_buf = vec![0; get_file_size(file)];
    let file_length = file.read(&mut file_content_buf).unwrap();
    file_content_buf.resize(file_length, 0);
    file_content_buf
}

#[allow(dead_code)]
fn get_file_string_content(file: &mut RegularFile) -> String {
    let file_content = get_file_raw_content(file);
    let mut u16_content = file_content.iter().map(|&u| u as u16).collect::<Vec<_>>();
    u16_content.push(0);
    let string_content = CStr16::from_u16_with_nul(&u16_content).unwrap();
    string_content.into()
}

#[entry]
fn main(_handle: Handle, mut system_table: SystemTable<Boot>) -> Status {
    uefi_services::init(&mut system_table).unwrap();
    system_table.stdout().clear().unwrap();
    info!("initialized");
    info!("UEFI revision: {}", system_table.uefi_revision());
    let key_event = unsafe { system_table.stdin().wait_for_key_event().unsafe_clone() };

    // get_memory_map(&mut system_table);

    // let handle = system_table
    //     .boot_services()
    //     .get_handle_for_protocol::<LoadedImage>()
    //     .unwrap();
    // let _loaded_image = system_table
    //     .boot_services()
    //     .open_protocol_exclusive::<LoadedImage>(handle)
    //     .unwrap();
    // let handle = system_table
    //     .boot_services()
    //     .get_handle_for_protocol::<DevicePath>()
    //     .unwrap();
    // let _device_path = system_table
    //     .boot_services()
    //     .open_protocol_exclusive::<DevicePath>(handle)
    //     .unwrap();
    let handle = system_table
        .boot_services()
        .get_handle_for_protocol::<SimpleFileSystem>()
        .unwrap();
    let mut file_system = system_table
        .boot_services()
        .open_protocol_exclusive::<SimpleFileSystem>(handle)
        .unwrap();

    let mut root = file_system.open_volume().unwrap();

    let root_handle = root.handle();
    let Ok(file_handle) =
        root_handle.open(cstr16!("kernel"), FileMode::Read, FileAttribute::default())
    else {
        todo!()
    };

    let mut file = file_handle.into_regular_file().unwrap();
    let file_content = get_file_raw_content(&mut file);
    file.close();

    let elf = elf::ElfBytes::<elf::endian::LittleEndian>::minimal_parse(&file_content).unwrap();
    let text_hdr = elf.section_header_by_name(".text").unwrap().unwrap();

    info!("{:X?}", text_hdr);

    let progam_range =
        (text_hdr.sh_offset) as usize..(text_hdr.sh_offset as usize + text_hdr.sh_size as usize);
    let program = &file_content.as_slice()[progam_range];
    info!("program: {:X?}", program);

    let main = program.as_ptr() as *const extern "C" fn() -> u8;
    let res = unsafe { *main }();
    info!("main exited with return value {res}");

    info!("press any key to continue");
    system_table
        .boot_services()
        .wait_for_event(&mut [key_event])
        .unwrap();
    system_table
        .runtime_services()
        .reset(ResetType::SHUTDOWN, Status::SUCCESS, None);
}
