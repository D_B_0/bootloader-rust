@echo off

cd bootloader

cargo build --release
if %errorlevel% neq 0 exit /b %errorlevel%

cd ..

copy target\x86_64-unknown-uefi\release\bootloader.efi OS\EFI\Boot\Bootx64.efi /y
if %errorlevel% neq 0 exit /b %errorlevel%

cd kernel

cargo build --release
if %errorlevel% neq 0 exit /b %errorlevel%

cd ..

copy target\x86_64-unknown-none\release\kernel OS\kernel /y
if %errorlevel% neq 0 exit /b %errorlevel%

qemu-system-x86_64.exe -bios .\OVMF\OVMF.fd -drive format=raw,file=fat:rw:.\OS
